<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $table = 'doctors_info';
    protected $primaryKey = 'doctor_id';
    
    public  $timestamps = true;
}
