<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'patients_info';
    protected $primaryKey = 'patient_id';
    
    public  $timestamps = true;
}
