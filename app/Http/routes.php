<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('d_signup_view', function () {
    return view('doctor/doctor_signup_view');
});

Route::get('p_signup_view', function () {
    return view('patient/patient_signup_view');
});

Route::get('authenticate_email', 'Common_Controller@authenticate_email');
Route::get('getCategories', 'Common_Controller@getCategories');
//Route::get('mypatientModal', function () {
//    return view('p_signup_model');
//});


Route::post('p_signup', 'Patient_Controller@store');




Route::get('set_appoinments', 'Doctor_Controller@set_appoinments');
