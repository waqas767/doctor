<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Hash;
use \Input;
use \DB;
use  \Validator;

use App\User;
use App\Patient;
use App\Doctor;


class Patient_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    
    
    public function store()
    {
        $user = new User;   
        $user->user_role = 2;
        $user->first_name = \Input::get('first_name');
        $user->last_name = \Input::get('last_name');
        $user->email = \Input::get('email');
        $user->password = Hash::make(\Input::get('password'));
        $user->phone = \Input::get('phone');
        $user->address = \Input::get('address');
        $user->city = \Input::get('city');
        $user->gender = \Input::get('gender');
        
        $success=DB::transaction(function() use ($user)
        {
            $user->save();
            return true;
        });
        if($success)
        {
            $patient = new Patient;   
            $patient->user_id = $user->id;
            $patient->date_of_birth = \Input::get('dobirth');
            
            $success=DB::transaction(function() use ($patient)
            {
                $patient->save();
                return true;
            });
            if($success)
            {
                echo 'Detail Saved';
            }
            else{
                echo 'Error Occoured';
            }
        }
        else
        {
            echo 'Error Occoured';
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
