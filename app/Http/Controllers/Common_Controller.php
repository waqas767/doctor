<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Hash;
use \Input;
use \DB;
use  \Validator;

use App\User;
use App\Patient;
use App\Doctor;

class Common_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategories() 
    {
        $categories = DB::table('categories')->select('*')->get();
        if(!$categories || $categories == '[]')
        {
            
            return \Response::json(array(
                    'status' => 'fail',
                    'categories' => '',
                        ), 200);
        }
        else
        {  
            return \Response::json(array(
                    'status' => 'success',
                    'categories' => $categories,
                        ), 200);
        }
    }
    
    public function authenticate_email() {
        $getvister = DB::table('users')->where('email', '=', $_GET['email'])->get();
        if ($getvister) {
            echo FALSE;
        } else {
            echo TRUE;
        }
    }
    
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
