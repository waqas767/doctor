<?php require('include/head.php'); ?>

<body id="page-top" class="index">
    <?php require('include/navigation.php'); ?>
    
    <?php require('include/header.php'); ?>
    

    <!-- Services Section -->
    <section id="about-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About Us</h2><br>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Increase access and utilization.</h4>
                    <p class="text-muted">Tap into the fastest-growing source of new patient acquisition. Every month, millions and trillion of patients search for new specialists and doctors on GoogDoc. Let them find your providers and book appointments instantly.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-search fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Meet a growing patient demand.</h4>
                    <p class="text-muted">Modern patients expect convenience and simplicity from their healthcare experience. Reduce leakage and prevent delay of care by giving patients instant access to your providers' schedules via our mobile app and across all of your online touch points.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-thumbs-up fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Improve patient experience.</h4>
                    <p class="text-muted">Did you know that people are more likely to recommend GoogDoc to their friends than they are to recommend Apple or Amazon? Turn our passionate community into your brand advocates while improving access for your existing patients.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2><br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
						<br>
                    <h4>Are you a Five-star Doctor?</h4>
					<p>Reach millions of patients</p>
					<p>List your practice on ZocDoc and deliver a better healthcare experience.</p>
					<ul>
					<li>Attract new patients to your office.</li>
					<li>Build and strengthen your online reputation.</li>
					<li>Offer the premium service your patients deserve.</li>
					</ul><br>
					<div  class="input-group">
						<a id="select-patient" href="#loginbox" style="width:100%;font-size:18px" class="btn btn-primary" 
						data-toggle="modal" data-dismiss="modal" data-target="#loginbox">List Your Practices</a>                                       
					</div>
<!-- 					<button id="loginbox" type="submit" class="btn btn-primary" data-toggle="modal">Login  </button>
 -->                </div>
                <div class="col-md-7 portfolio-item">
                    <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/service-bg.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2009-2011</h4>
                                    <h4 class="subheading">Our Humble Beginnings</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>March 2011</h4>
                                    <h4 class="subheading">An Agency is Born</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>December 2012</h4>
                                    <h4 class="subheading">Transition to Full Service</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/4.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>July 2014</h4>
                                    <h4 class="subheading">Phase Two Expansion</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Be Part
                                    <br>Of Our
                                    <br>Story!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
   
     <!-- Contact Section -->
    <!--<section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contact Us</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
 -->
        <div class="container goog-footer" id="contact">
			<div class="row">
				<div class="col-md-4 goog-social-footer"><br>
					<a href="#"><i class="fa fa-facebook fa-fw-ft fa-4x"></i></a>
					<a href="#"><i class="fa fa-twitter fa-fw-ft fa-4x"></i></a>
					<a href="#"><i class="fa fa-linkedin fa-fw-ft fa-4x"></i></a>
					<a href="#"><i class="fa fa-google-plus fa-fw-ft fa-4x"></i></a>
				</div>
				<div class="col-md-4 goog-contact-footer">
					<div style="margin-bottom: 25px" class="input-group">
						<span><i class="glyphicon glyphicon-envelope"></i></span>
						<label type="text"> mannee236@gmail.com</label>
					</div>
					<div style="margin-bottom: 25px" class="input-group">
						<span><i class="glyphicon glyphicon-phone"></i></span>
						<label type="text"> +92-3204004064</label>
					</div>
				</div>
				<div class="col-md-4 goog-site-map-footer">
					<h3>Site Map </h3>
					<a class="page-scroll" href="#about-us">About</a><br><br>
					<a href="#mychoiceModal" data-toggle="modal">Join GoogDoc Today</a>
				</div><br>
			</div>
			<p>&copy;2015, UziTech PK</p>
		</div>

    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal modal fade"  style="overflow:scroll" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Project Name</h2>
                            <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                            <img class="img-responsive img-centered" src="img/service-bg.png" alt="">
                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                            <p>
                                <strong>Want these icons in this portfolio item sample?</strong>You can download 60 of them for free, courtesy of <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">RoundIcons.com</a>, or you can purchase the 1500 icon set <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">here</a>.</p>
                            <ul class="list-inline">
                                <li>Date: July 2014</li>
                                <li>Client: Round Icons</li>
                                <li>Category: Graphic Design</li>
                            </ul>
                            <button type="button" class="btn btn-primary close-project" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require('models/login_model.php'); ?>
	
	<!--Choice modal : Doctor Vs Patient-->
	
	<div class="portfolio-modal modal fade" style="overflow:scroll" id="mychoiceModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                    <div class="panel-title" style="text-align:center">Select Your Category</div>
                            </div>     

                            <div style="padding-top:30px" class="panel-body" >

                                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                                <form id="loginform col-md-6" class="form-horizontal" role="form">
                                                        <br>
                                    <div style="margin-bottom: 0px;width:100%;padding:16px;" class="input-group">
                                            <a id="select-patient" href="<?php echo asset('p_signup_view') ?>" style="width:100%;font-size:18px" class="btn btn-primary">Patient </a>                                          
                                    </div>
                                            <h3 style="text-align:center">OR</h3>
                                    <div style="margin-bottom: 38px;width:100%;padding:16px;" class="input-group">
                                            <a id="select-doctor" href="<?php echo asset('d_signup_view') ?>" style="width:100%;font-size:18px" class="btn btn-primary">Doctor </a>                                       
                                    </div>

                                </form>     
                            </div>                     
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-primary close-project" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>

            </div>
        </div>
    </div>

 
        
        
        
        
<?php require('models/p_signup_model.php'); ?>
<?php require('models/d_signup_model.php'); ?>
<script>
    $('#loginform').validate({// initialize the plugin
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        
                        password: {
                            required: true,
//                            alphanumeric:true,
                            minlength: 8
                        },
                        
                    },
                    messages: {
                        password: {
                            required: "",
                            minlength: "Your password must be at least 8 characters long"
                        },
                        email: "Invalid email address",
                    
                    }


                });
</script>
 



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
<!--    <script src="js/contact_me.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

</body>

</html>
