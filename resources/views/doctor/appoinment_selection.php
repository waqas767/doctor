<div id="form">
<!--    <form id="basicExample">
        <input type="text" class="date start" />
        <input type="text" class="time start" /> to
        <input type="text" class="time end" />
    </form>-->
</div>

<script src="<?php echo asset('js/jquery.js')?>"></script>

<!-- include input widgets; this is independent of Datepair.js -->
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/jquery.timepicker.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap-datepicker.css')?>" />
<script type="text/javascript" src="<?php echo asset('js/bootstrap-datepicker.js')?>"></script>
<script src="<?php echo asset('js/jquery.timepicker.js')?>"></script>

<script type="text/javascript" src="<?php echo asset('js/datepair.js')?>"></script>
<style>
    .start_to_end{
            display: inline;
    }
</style>

<script>
    
    // initialize input widgets first
    $( document ).ready(function() {
        
        var i = 0;
        var count = add_slot(i);
        add_slot_button();
        
        
   
        
        
        var add_btn = document.getElementById('add_slot');
        add_btn.onclick = function () 
        {
            var date = document.getElementById('date_'+count).value;
            var start_time = document.getElementById('start_time_'+count).value;
            var end_time = document.getElementById('end_time_'+count).value;
            
            if(date != "" && start_time != "" && end_time != "")
            {
                console.log("Date: "+date+" Start Time: "+start_time+" End Time: "+end_time);
            }
            else
            {
                if(date == "")
                {
                    alert("Please select date.");
                    return;
                }
                if(start_time == "")
                {
                    alert("Please select start time.");
                    return;
                }
                if(end_time == "")
                {
                    alert("Please select end time.");
                    return;
                }
            }
            
            count++;
            add_slot(count);
        };
        
        
    });
    
    function add_slot(i)
    {
        var count = i;
        var p = document.createElement("p");
        p.setAttribute('id',"basicExample");

        var date = document.createElement("input"); //input element, text
        date.setAttribute('type',"text");
        date.setAttribute('name',"date");
        date.setAttribute('class',"date start");
        date.setAttribute('id',"date_"+count);

        var start_time = document.createElement("input"); //input element, Submit button
        start_time.setAttribute('type',"text");
        start_time.setAttribute('name',"start_time");
        start_time.setAttribute('class',"time start");
        start_time.setAttribute('id',"start_time_"+count);
        
        var p1 = document.createElement("p");
        p1.setAttribute('class',"start_to_end");
        p1.setAttribute('id',"p_"+count);
        p1.textContent = ' To ';
        
        var end_time = document.createElement("input"); //input element, Submit button
        end_time.setAttribute('type',"text");
        end_time.setAttribute('name',"end_time");
        end_time.setAttribute('class',"time end");
        end_time.setAttribute('id',"end_time_"+count);

        p.appendChild(date);
        p.appendChild(start_time);
        p.appendChild(p1);
        p.appendChild(end_time);

        //and some more input elements here
        //and dont forget to add a submit button

        document.getElementById('form').appendChild(p);
        
        
        $('#basicExample .time').timepicker({
            'showDuration': true,
            'timeFormat': 'g:ia',
//            'minTime': '1:30am'
        });

        $('#basicExample .date').datepicker({
            'format': 'm/d/yyyy',
            'autoclose': true,
            'startDate': new Date()
        });

        // initialize datepair
        var basicExampleEl = document.getElementById('basicExample');
        var datepair = new Datepair(basicExampleEl);
        
        
        return count;
    }
    
    function add_slot_button()
    {
        //Create an input type dynamically.   
        var element = document.createElement("input");
        //Assign different attributes to the element. 
        element.setAttribute("type", "submit");
        element.setAttribute("value", "Add New");
        element.setAttribute("name", "add_slot");
        element.setAttribute("id", "add_slot");
        
        document.getElementById('basicExample').appendChild(element);
    }
    
    
</script>

