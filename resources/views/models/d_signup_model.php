	
	<!--Doctor Sign up: Modal-->
         <script type="text/javascript">
        function initialize() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "pk"}
            };
            var input = document.getElementById('cities');
            var autocomplete = new google.maps.places.Autocomplete(input , options);
            
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	
<div class="portfolio-modal modal fade" style="overflow:scroll" id="doctorModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                                <div class="panel-title">Sign Up</div>
                        </div>  
                        <div class="panel-body" >
                            <div class="row">
                                <form id="signupform" class="form-horizontal" role="form">
                                    <div class="col-md-6">

                                        <div id="signupalert" style="display:none" class="alert alert-danger">
                                            <p>Error:</p>
                                            <span></span>
                                        </div>

                                        <div class="form-group" >
                                            <label for="email" class="col-md-4 control-label">First Name</label>
                                            <div class="col-md-8" >
                                                <input type="text" class="form-control modal-input-field" name="first_name" id="first_name" placeholder="First Name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="last_name" id="last_name" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                                <label for="email" class="col-md-4 control-label">Email</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control modal-input-field" name="email" id="email" placeholder="Email">
                                                    <span id="email_error" style="width: auto;display: inline;color:red;font-size: 12px;float:center; display: none;">Email already exist.</span>
                                                </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="password" class="col-md-4 control-label">Password</label>
                                            <div class="col-md-8">
                                                <input type="password" class="form-control modal-input-field" name="password" id="password" placeholder="Password" title="Password must be Alphanumeric. i.e, abc123">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="ph-no" class="col-md-4 control-label">Phone No</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="phone" id="phone" placeholder="+92">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="ph-no" class="col-md-4 control-label">Check up Plan</label>
                                            <div class="col-md-8">
                                                <div class="col-md-6" style="margin-top: 10px">
                                                    <div class="checkbox" data-toggle="collapse" href="#home-visit" >
                                                      <label>
                                                        <input type="checkbox"   name="optionsRadios" id="home-plan" value="option1" >Home</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="margin-top: 10px">
                                                    <div class="checkbox" data-toggle="collapse" href="#hospital-visit" >
                                                        <label>
                                                            <input type="checkbox" name="optionsRadios" id="hospital-plan" value="option1" >Hospital</label>
                                                    </div>
                                                </div>
                                                    <div class="collapse col-md-12" style="margin-top:8px" id="home-visit">
                                                        <div class="col-md-6">  
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                                                    5Km
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" >
                                                                    10Km
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="radio">
                                                                 <label>
                                                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" >
                                                                    15Km
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" >
                                                                    20Km
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="collapse col-md-12" style="margin-top:8px;" id="hospital-visit">
                                                      <input type="text" class="form-control modal-input-field" name="hospital-address" placeholder="Hospital Address">
                                                    </div>
                                                </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group" >
                                            <label for="address" class="col-md-4 control-label">Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="address" placeholder="Address">
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="city" class="col-md-4 control-label">City</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="city" id="cities" placeholder="City" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="gender" class="col-md-4 control-label">Gender</label>
                                            <div class="col-md-8">
                                                <select name="gender" class="form-control modal-input-field" id="sel1">
                                                    <option class="dropdown-content" value="">Select</option>
                                                    <option class="dropdown-content" value="male">Male</option>
                                                    <option class="dropdown-content" value="female">Female</option>>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="dobirth" class="col-md-4 control-label">DOB</label>
                                            <div class="col-md-8">
                                                <input type="date" class="form-control modal-input-field" name="dobirth" placeholder="Password">
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="reg-no" class="col-md-4 control-label">Reg No</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="registration" id="registration" placeholder="Registration No">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="exampleInputFile"class="col-md-4 control-label" style="margin-top: 3px">Picture</label>
                                            <div class="col-md-8" style="margin-top: 10px">
                                                <input type="file" id="exampleInputFile">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group" action="#page-top">
                                        <!-- Button -->                                        
                                        <div class=" col-md-4 col-md-offset-4">
<!--                                                <button  type="submit" class="btn btn-primary btn-lg"><i class="icon-hand-right"></i>  Sign Up</button>-->
                                            <input type="submit" class="btn btn-primary" value="Submit" id="submitform">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <button type="button" class="btn btn-primary close-project" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                </div>
            </div>
        </div>
    </div>
</div>
    
    <!--</div>-->
<style>
    input.error {
        border:solid 1px red !important;
    }
    #signupform checkbox.error {
       background-color: red;
    }
    #signupform label.error {
        width: auto;
        display: inline;
        color:red;
        font-size: 12px;
        float:right;
    }
</style>

    
    <script type="text/javascript">
        $(document).ready(function() {
            
            $('#email').focusout(function() {
                    email = $(this).val();
                    $('#regloader').fadeIn();
                    $.ajax({
                        type: "GET",
                        data: {"email": email},
                        url: "<?php echo asset('authenticate_email'); ?>",
                        success: function(data) {
//                            $("#regloader").fadeOut(1000);
                            if (data) {
                                alert('success');
                                $('#email').css('border-color', 'black');
                                $('#email_error').css('display', 'none');
                                $('#submitform').prop('disabled', false);
//                                $("#patientSignupForm").submit(function(){
//                                    return false;
//                                });
                            } else {
                                alert('error');
                                $('#email').css('border-color', 'red');
                                $('#email_error').css('display', 'block');
                                $('#submitform').prop('disabled', 'disabled');
//                                $("#patientSignupForm").submit(function(){
//                                    return false;
//                                });
                            }
                        }
                    });
                });
            
           
            
            
            
            $( "#signupform :input" ).tooltip( "disable" );
            
            $( ".selector" ).tooltip( "option", "position", { my: "left top+15", at: "left bottom", collision: "flipfit" } );

//            $( "#patientSignupForm :input" ).tooltip({
//                position: { my: "left top+15", at: "left bottom", collision: "flipfit" }
//              });

            // select all desired input fields and attach tooltips to them
            $("#signupform :input").tooltip({
                
            // place tooltip on the right edge
            position: { my: "left top+15", at: "left bottom", collision: "flipfit" },

            // a little tweaking of the position
//            offset: [-2, 10],
//
//            // use the built-in fadeIn/fadeOut effect
//            effect: "fade",
//
//            // custom opacity setting
//            opacity: 0.7,
////            close: function( event, ui ) { 
////                $(this).tooltip('disable'); 
////                /* instead of $(this) you could also use $(event.target) */
////            }
//            
//            
//
            });
            
            $('#password').focusout(function() {
             
                passowrd2 = $(this).val();
                
                if (! passwordre (passowrd2)) {
                   
                    $('#submitform').attr('disabled', true);
                    $('#password').css("border","solid 1px red");
                    $( "#signupform :input" ).tooltip( "enable" );
                } 
                else {
                    $('#submitform').attr('disabled', false);
                    $('#password').css("border","solid 1px gray");
                    $( "#signupform :input" ).tooltip( "disable" );
                }
            }); 
            function passwordre(password) {
                var regex = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;
                return regex.test(password);
            }
            
        });
    </script>

 <script type="text/javascript">

//            $(document).ready(function() {
                $('#signupform').validate({// initialize the plugin
                    rules: {
                        first_name: "required",
                        last_name: "required",
                        city:"required",
                        address:"required",
                        registration:"required",
                        gender: { 
                            required: true 
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        
                        phone: {
                            required: true,
                            digits: true,
                            minlength: 10,
                            maxlength: 13
                        },
                        
                        password: {
                            required: true,
//                            alphanumeric:true,
                            minlength: 8
                        },
                        confrim_password: {
                            required: true,
                            minlength: 8,
                            equalTo: "#password"
                        },
                        dobirth: {
                            required: true,
                            date: true
                        }
                    },
                    messages: {
                        first_name: "Enter your first name",
                        last_name: "Enter your last name",
                        city: "Enter your city name",
                        registration:"Enter your registration no.",
                        address:"Enter your home address",
                        gender: { 
                            required: "Select your gender" 
                        },
                        email: {
                            required: "Enter your email.",
                            email: "Enter valid email."
                        },
                        password: {
                            required: "Enter your password",
                            minlength: "Your password must be at least 8 characters long"
                        },
                        
                        confrim_password: {
                            required: "Please confirm the password.",
                            minlength: "Your password must be at least 8 characters long",
                            equalTo: "Please enter the same password as above"
                        },
                        phone: {
                            required: "Enter your phone number",
                            digits: "Please enter a valid phone number"
                            
                        },
                        dobirth: {
                            required: "Select your Date of birth"
                            
                        }
                    }



                });
           
            
        </script>