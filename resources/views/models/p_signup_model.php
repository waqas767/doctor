
<!--Patient Sign up : Modal-->
 <script type="text/javascript">
        function initialize() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "pk"}
            };
            var input = document.getElementById('cities');
            var autocomplete = new google.maps.places.Autocomplete(input , options);
            
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<div class="portfolio-modal modal fade" style="overflow:scroll" id="mypatientModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-content ">
    <div class="close-modal " data-dismiss="modal">
        <div class="lr">
            <div class="rl">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">Sign Up</div>
                    </div>  
                    <div class="panel-body" >
                        <form method="post" action="<?php echo asset('p_signup') ?>" id="patientSignupForm" class="form-horizontal">
                            <div class="form-group" >
                                <label for="firstname" class="col-md-4 control-label ">First Name</label>
                                <div class="col-md-6" >
                                    <input type="text" class="form-control modal-input-field" name="first_name" id="first_name" placeholder="First Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control modal-input-field" name="last_name" id="last_name" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="email" class="col-md-4 control-label">Email</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control modal-input-field" name="email" id="email" placeholder="Email" >
                                    <span id="email_error" style="width: auto;display: inline;color:red;font-size: 12px;float:center; display: none;">Email already exist.</span>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="password" class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control modal-input-field" name="password" id="password" placeholder="Password"  title="Password must be Alphanumeric. i.e, abc123">
                                </div>
                            </div>
                            
                            <div class="form-group" >
                                <label for="confrim_password" class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control modal-input-field" name="confrim_password" id="confrim_password" placeholder="Password">
                                </div>
                            </div>

                            <div class="form-group">
                                    <label for="address" class="col-md-4 control-label">Address</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control modal-input-field" name="address" id="address" placeholder="Address">
                                    </div>
                            </div>

                            <div class="form-group" >
                                <label for="cities" class="col-md-4 control-label">City</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control modal-input-field" name="city" id="cities" placeholder="City" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="gender" class="col-md-4 control-label">Gender</label>
                                <div class="col-md-6">
                                    <select name="gender" class="form-control modal-input-field" id="sel1">
                                        <option class="dropdown-content" value="">Select</option>
                                        <option class="dropdown-content" value="male">Male</option>
                                        <option class="dropdown-content" value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="dobirth" class="col-md-4 control-label">Date of Birth</label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control modal-input-field" name="dobirth" id="dobirth">
                                </div>
                            </div>

                            <div class="form-group" >
                                <label for="phone" class="col-md-4 control-label">Phone No</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control modal-input-field" name="phone" id="phone" placeholder="+92">
                                </div>
                            </div>

                            <div class="form-group">
                                <!-- Button -->                                        
                                <div class=" col-md-4 col-md-offset-4">
<!--                                    <button id="btn-signup" type="submit" class="btn btn-primary"><i class="icon-hand-right"></i>  Sign Up</button>-->
                                    <input type="submit" class="btn btn-primary" value="Submit" id="submitform">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <button type="button" class="btn btn-primary close-project" data-dismiss="modal">Close</button>
            </div>
        </div>
<!--    </div>
</div>-->
<!--</div>-->
<style>
            input.error {
                border:solid 1px red !important;
            }
            #patientSignupForm checkbox.error {
               background-color: red;
            }
            #patientSignupForm label.error {
                width: auto;
                display: inline;
                color:red;
                font-size: 12px;
                float:right;
            }
        </style>
        
    <style>
/*        .tooltip {
            background-color:#000;
            border:1px solid #fff;
            padding:10px 15px;
            width:200px;
            display:none;
            color:#fff;
            text-align:left;
            font-size:12px;

             outline radius for mozilla/firefox only 
            -moz-box-shadow:0 0 10px #000;
            -webkit-box-shadow:0 0 10px #000;
        }*/
    </style>
    
    <script type="text/javascript">
        $(document).ready(function() {
            
            $('#email').focusout(function() {
                    email = $(this).val();
                    $('#regloader').fadeIn();
                    $.ajax({
                        type: "GET",
                        data: {"email": email},
                        url: "<?php echo asset('authenticate_email'); ?>",
                        success: function(data) {
//                            $("#regloader").fadeOut(1000);
                            if (data) {
                                alert('success');
                                $('#email').css('border-color', 'black');
                                $('#email_error').css('display', 'none');
                                $('#submitform').prop('disabled', false);
//                                $("#patientSignupForm").submit(function(){
//                                    return false;
//                                });
                            } else {
                                alert('error');
                                $('#email').css('border-color', 'red');
                                $('#email_error').css('display', 'block');
                                $('#submitform').prop('disabled', 'disabled');
//                                $("#patientSignupForm").submit(function(){
//                                    return false;
//                                });
                            }
                        }
                    });
                });
            
           
            
            
            
            $( "#patientSignupForm :input" ).tooltip( "disable" );
            
            $( ".selector" ).tooltip( "option", "position", { my: "left top+15", at: "left bottom", collision: "flipfit" } );

//            $( "#patientSignupForm :input" ).tooltip({
//                position: { my: "left top+15", at: "left bottom", collision: "flipfit" }
//              });

            // select all desired input fields and attach tooltips to them
            $("#patientSignupForm :input").tooltip({
                
            // place tooltip on the right edge
            position: { my: "left top+15", at: "left bottom", collision: "flipfit" },

            // a little tweaking of the position
//            offset: [-2, 10],
//
//            // use the built-in fadeIn/fadeOut effect
//            effect: "fade",
//
//            // custom opacity setting
//            opacity: 0.7,
////            close: function( event, ui ) { 
////                $(this).tooltip('disable'); 
////                /* instead of $(this) you could also use $(event.target) */
////            }
//            
//            
//
            });
            
            $('#password').focusout(function() {
             
                passowrd2 = $(this).val();
                
                if (! passwordre (passowrd2)) {
                   
                    $('#submitform').attr('disabled', true);
                    $('#password').css("border","solid 1px red");
                    $( "#patientSignupForm :input" ).tooltip( "enable" );
                } 
                else {
                    $('#submitform').attr('disabled', false);
                    $('#password').css("border","solid 1px gray");
                    $( "#patientSignupForm :input" ).tooltip( "disable" );
                }
            }); 
            function passwordre(password) {
                var regex = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;
                return regex.test(password);
            }
            
        });
    </script>

 <script type="text/javascript">

//            $(document).ready(function() {
                $('#patientSignupForm').validate({// initialize the plugin
                    rules: {
                        first_name: "required",
                        last_name: "required",
                        country_1: "required",
                        city:"required",
                        address:"required",
//                        state_1:"required",
                        gender: { 
                            required: true 
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        
                        phone: {
                            required: true,
                            digits: true,
                            minlength: 10,
                            maxlength: 13
                        },
                        
                        password: {
                            required: true,
//                            alphanumeric:true,
                            minlength: 8
                        },
                        confrim_password: {
                            required: true,
                            minlength: 8,
                            equalTo: "#password"
                        },
                        dobirth: {
                            required: true,
                            date: true
                        }
                    },
                    messages: {
                        first_name: "Enter your first name",
                        last_name: "Enter your last name",
                        city: "Enter your city name",
                        address:"Enter your home address",
                        gender: { 
                            required: "Select your gender" 
                        },
                        email: {
                            required: "Enter your email.",
                            email: "Enter valid email."
                        },
                        password: {
                            required: "Enter your password",
                            minlength: "Your password must be at least 8 characters long"
                        },
                        
                        confrim_password: {
                            required: "Please confirm the password.",
                            minlength: "Your password must be at least 8 characters long",
                            equalTo: "Please enter the same password as above"
                        },
                        phone: {
                            required: "Enter your phone number",
                            digits: "Please enter a valid phone number"
                            
                        },
                        dobirth: {
                            required: "Select your Date of birth"
                            
                        }
                    }


                });
           
            
        </script>