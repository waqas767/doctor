	
<!-- Portfolio Modal 1 -->
<div class="portfolio-modal modal fade" style="overflow:scroll" id="loginbox" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-info" >
                        <div class="panel-heading">
                            <div class="panel-title">Sign In</div>
                            <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                        </div>     

                        <div style="padding-top:30px" class="panel-body" >
                            <div style="display:none" id="login-alert" class="alert alert-danger col-md-6"></div>
                            <form id="loginform" action="<?php echo asset('p_signup') ?>" class="form-horizontal" role="form">

                                <div class="form-group" >
                                    <label for="email" class="col-md-3 control-label ">Email</label>
                                    <div class="col-md-8" >
                                        <input type="email" class="form-control modal-input-field" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label for="email" class="col-md-3 control-label ">Password</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control modal-input-field" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input style="text-align:left" id="login-remember" type="checkbox" name="remember" value="1"> Remember me</input>
                                </div>



                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls">
<!--                                        <button id="btn-login" type="submit" class="btn btn-primary">Login  </button>-->
                                            <input type="submit" class="btn btn-primary" value="Login" id="submitform">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#222222; padding-top:26px; font-size:100%" >
                                            Don't have an account! 
                                            <a href="#mychoiceModal" data-toggle="modal" data-dismiss="modal">Sign Up Here</a>
                                        </div>
                                     
                                    </div>
                                </div>    
                            </form>     
                        </div>                     
                    </div>  
                </div>
            </div>
        </div>
        <button type="button" class="btn btn-primary close-project" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
    </div>
</div>