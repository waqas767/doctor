<?php require('include/head.php'); ?>

    <body>
        <!-- Navigation -->
		<div class="container">
        <div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
    <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand" href="#">Brand</a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-menubuilder">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Home</a>
                </li>
                <li><a href="#">About</a>
                </li>
                <li><a href="#">Help</a>
                </li>
                <li><a href="#myModal" data-toggle="modal" data-target="#myModal" data-dismiss="Modal">Sign In</a>
                </li>
                <li><a href="#mychoiceModal" data-toggle="modal" data-target="#mychoiceModal" data-dismiss="Modal">Sign Up</a>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>


<!-- Login Modal -->
<?php require('models/login_model.php'); ?>

<!-- Signup Modal -->
<?php require('models/Signup_model.php'); ?>
<!--Select your category as a doctor or patient:Model-->

<!--data-backdrop="static" data-keyboard="false" -->


    </body>
</html>