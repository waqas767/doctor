<!-- Header -->
<header>
    <div class="container">
        <div class="intro-text">
            <h1>Find a doctor you love.</h1>
            <div class="row search-doctor">
                <div class="col-md-3">
                        <form method="post">
                                <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon search-addon"><i class="glyphicon glyphicon-check"></i></span>
                                        <select class="form-control input-select-field" id="sel1">
                                                <option class="dropdown-content">Chose a Speciality</option>
                                                <option class="dropdown-content">Male</option>
                                                <option class="dropdown-content">Female</option>
                                        </select>
                                </div>
                        </form>
                </div>
                <div class="col-md-3">
                        <form method="post">
                                <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon search-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                        <input id="cities" type="text" class="form-control input-form-field" name="cities" placeholder="Enter City Name">
                                </div>
                        </form>
                </div>
                <div class="col-md-4">
                        <form method="post">
                                <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon search-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <select class="form-control input-select-field" id="sel1">
                                                <option class="dropdown-content">I'll choose my insurance later</option>
                                                <option class="dropdown-content">Male</option>
                                                <option class="dropdown-content">Female</option>
                                        </select>
                                </div>
                        </form>
                </div>
                <div class="col-md-2">
                        <form method="post">
                                <button class="btn btn-primary btn-xl">Search</button>
                        </form>
                </div>
            </div>
            <h1>Get the care you need.</h1><br><br>   
        </div>
    </div>
</header>

