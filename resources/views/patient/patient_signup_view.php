<?php require('/../include/head.php'); ?>



<body id="page-top" class="index">
    <?php require('/../include/navigation.php'); ?>
    
    
    <div class="container" style="margin: 105px auto">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                                <div class="panel-title">Sign Up</div>
                        </div>  
                        <div class="panel-body" >
                            <div class="row">
                                <form id="signupform" action="<?php echo asset('p_signup') ?>" class="form-horizontal" method="post">
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <label for="email" class="col-md-4 control-label">First Name</label>
                                            <div class="col-md-8" >
                                                <input type="text" class="form-control modal-input-field" name="first_name" id="first_name" placeholder="First Name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="last_name" id="last_name" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                                <label for="email" class="col-md-4 control-label">Email</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control modal-input-field" name="email" id="email" placeholder="Email">
                                                    <span id="email_error" class="form-validation-error">Email already exist.</span>
                                                </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="password" class="col-md-4 control-label">Password</label>
                                            <div class="col-md-8">
                                                <input type="password" class="form-control modal-input-field" name="password" id="password" placeholder="Password">
                                                <span id="password_error" class="form-validation-error">Password must be Alphanumeric. i.e, abc123</span>
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="confrim_password" class="col-md-4 control-label">Confirm Password</label>
                                            <div class="col-md-8">
                                                <input type="password" class="form-control modal-input-field" name="confrim_password" id="confrim_password" placeholder="Password" title="Password must be Alphanumeric. i.e, abc123">
                                            </div>
                                        </div>
                                        

                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group" >
                                            <label for="address" class="col-md-4 control-label">Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="address" placeholder="Address">
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="city" class="col-md-4 control-label">City</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="city" id="cities" placeholder="City" autocomplete='off'>
<!--                                                <input id="cities" autocomplete="off" type="text" class="form-control input-form-field" name="cities" placeholder="Enter City Name">-->
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="gender" class="col-md-4 control-label">Gender</label>
                                            <div class="col-md-8">
                                                <select name="gender" class="form-control modal-input-field" id="sel1">
                                                    <option class="dropdown-content" value="">Select</option>
                                                    <option class="dropdown-content" value="male">Male</option>
                                                    <option class="dropdown-content" value="female">Female</option>>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="dobirth" class="col-md-4 control-label">DOB</label>
                                            <div class="col-md-8">
                                                <input type="date" class="form-control modal-input-field" name="dobirth" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="ph-no" class="col-md-4 control-label">Phone No</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control modal-input-field" name="phone" id="phone" placeholder="+92">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <!-- Button -->                                        
                                        <div class=" col-md-4 col-md-offset-4">
<!--                                                <button  type="submit" class="btn btn-primary btn-lg"><i class="icon-hand-right"></i>  Sign Up</button>-->
                                            <input type="submit" class="btn btn-primary" value="Submit" id="submitform">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<!--                <button type="button" class="btn btn-primary close-project" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>-->
                </div>
            </div>
        </div>
    </div>
</div>
    
    <!--</div>-->
<style>
    input.error {
        border:solid 1px red !important;
    }
    #signupform checkbox.error {
       background-color: red;
    }
    #signupform label.error {
        width: auto;
        display: inline;
        color:red;
        font-size: 12px;
        float:right;
    }
</style>

<script type="text/javascript">
        $(document).ready(function() {
            
            $('#email').focusout(function() {
                    email = $(this).val();
                    $('#regloader').fadeIn();
                    $.ajax({
                        type: "GET",
                        data: {"email": email},
                        url: "<?php echo asset('authenticate_email'); ?>",
                        success: function(data) {
//                            $("#regloader").fadeOut(1000);
                            if (data) {
                                $('#email').css('border-color', 'black');
                                $('#email_error').css('display', 'none');
                                $('#submitform').prop('disabled', false);
//                             
                            } else {
                                $('#email').css('border-color', 'red');
                                $('#email_error').css('display', 'block');
                                $('#submitform').prop('disabled', 'disabled');
//                            
                            }
                        }
                    });
                });
            $('#password').focusout(function() {
             
                passowrd2 = $(this).val();
                
                if (! passwordre (passowrd2)) {
                   
                    $('#submitform').attr('disabled', true);
                    $('#password').css("border","solid 1px red");
                    $('#password_error').css('display', 'block');
                } 
                else {
                    $('#submitform').attr('disabled', false);
                    $('#password').css("border","solid 1px black");
                    $( "#signupform :input" ).tooltip( "disable" );
                    $('#password_error').css('display', 'none');
                }
            }); 
            function passwordre(password) {
                var regex = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;
                return regex.test(password);
            }
            
        });
    </script>

 <script type="text/javascript">

//            $(document).ready(function() {
                $('#signupform').validate({// initialize the plugin
                    rules: {
                        first_name: "required",
                        last_name: "required",
                        city:"required",
                        address:"required",
                        registration:"required",
                        gender: { 
                            required: true 
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        
                        phone: {
                            required: true,
                            digits: true,
                            minlength: 10,
                            maxlength: 13
                        },
                        
                        password: {
                            required: true,
//                            alphanumeric:true,
                            minlength: 8
                        },
                        confrim_password: {
                            required: true,
                            minlength: 8,
                            equalTo: "#password"
                        },
                        dobirth: {
                            required: true,
                            date: true
                        }
                    },
                    messages: {
                        first_name: "Enter your first name",
                        last_name: "Enter your last name",
                        city: "Enter your city name",
                        registration:"Enter your registration no.",
                        address:"Enter your home address",
                        gender: { 
                            required: "Select your gender" 
                        },
                        email: {
                            required: "Enter your email.",
                            email: "Enter valid email."
                        },
                        password: {
                            required: "Enter your password",
                            minlength: "Your password must be at least 8 characters long"
                        },
                        
                        confrim_password: {
                            required: "Please confirm the password.",
                            minlength: "Your password must be at least 8 characters long",
                            equalTo: "Please enter the same password as above"
                        },
                        phone: {
                            required: "Enter your phone number",
                            digits: "Please enter a valid phone number"
                            
                        },
                        dobirth: {
                            required: "Select your Date of birth"
                            
                        }
                    }



                });
           
            
        </script>
  <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
<!--    <script src="js/contact_me.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>  

</body>

</html>
